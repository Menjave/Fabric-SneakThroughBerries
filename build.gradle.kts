plugins {
    id("fabric-loom")
    val kotlinVersion: String by System.getProperties()
    kotlin("jvm").version(kotlinVersion)
}

base {
    val archivesBaseName: String by project
    archivesName.set(archivesBaseName)
}

val modVersion: String by project
version = modVersion
val mavenGroup: String by project
group = mavenGroup
minecraft {}
repositories {
    maven("https://maven.shedaniel.me/")
}

/*
* I might be entirely new to this stuff, but I was just having non-stop trouble with getting gradle to work properly.
* It would not read project properly and it just kept breaking. So this is the workaround.
*/

val minecraft_Version = "1.18"
val yarn_Mappings = "1.18+build.1"
val loader_Version = "0.12.9"
val fabric_Version = "0.44.0+1.18"
val fabricKotlinVersion = "1.7.0+kotlin.1.6.0"
val ClothConVar = "6.0.45"

dependencies {
    minecraft("com.mojang:minecraft:${minecraft_Version}")
    mappings("net.fabricmc:yarn:${yarn_Mappings}:v2")

    modImplementation("net.fabricmc:fabric-loader:${loader_Version}")
    modImplementation("net.fabricmc.fabric-api:fabric-api:${fabric_Version}")
    modImplementation("net.fabricmc:fabric-language-kotlin:${fabricKotlinVersion}")
    annotationProcessor("net.fabricmc:fabric-mixin-compile-extensions:0.4.6")
    modImplementation("me.shedaniel.cloth:cloth-config-fabric:${ClothConVar}") {
        exclude("net.fabricmc.fabric-api")
    }
}
tasks {
    val javaVersion = JavaVersion.VERSION_17
    withType<JavaCompile> {
        options.encoding = "UTF-8"
        sourceCompatibility = javaVersion.toString()
        targetCompatibility = javaVersion.toString()
        options.release.set(javaVersion.toString().toInt())
    }
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions { jvmTarget = javaVersion.toString() }
        sourceCompatibility = javaVersion.toString()
        targetCompatibility = javaVersion.toString()
    }
    jar { from("LICENSE") { rename { "${it}_${base.archivesName}" } } }
    processResources {
        inputs.property("version", project.version)
        filesMatching("fabric.mod.json") { expand(mutableMapOf("version" to project.version)) }
    }
    java {
        toolchain { languageVersion.set(JavaLanguageVersion.of(javaVersion.toString())) }
        sourceCompatibility = javaVersion
        targetCompatibility = javaVersion
        withSourcesJar()
    }
}