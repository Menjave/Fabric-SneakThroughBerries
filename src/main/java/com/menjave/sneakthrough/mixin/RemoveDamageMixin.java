package com.menjave.sneakthrough.mixin;

import com.menjave.sneakthrough.SneakConfig;
import net.minecraft.block.BlockState;
import net.minecraft.block.PlantBlock;
import net.minecraft.block.SweetBerryBushBlock;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;



@Mixin(SweetBerryBushBlock.class)
abstract public class RemoveDamageMixin extends PlantBlock {

    public RemoveDamageMixin(Settings settings) {
        super(settings);
    }

    @Inject(
            method = "onEntityCollision",
            at = @At(
                    value = "INVOKE",
                    target = "Lnet/minecraft/entity/Entity;damage(Lnet/minecraft/entity/damage/DamageSource;F)Z"
            ),
            cancellable = true
    )
    private void sneakthrough_onEntityCollision(
            BlockState stat,
            World world,
            BlockPos pos,
            Entity entity,
            CallbackInfo ci
    ) {

        if (entity instanceof PlayerEntity player) {

            if (player.isSneaking() && Config.INSTANCE.getSneakToStopDamage()) {
                ci.cancel();
                return;
            }

            ItemStack feet = player.getEquippedStack(EquipmentSlot.FEET);
            ItemStack legs = player.getEquippedStack(EquipmentSlot.LEGS);
            ItemStack chest = player.getEquippedStack(EquipmentSlot.CHEST);
            ItemStack head = player.getEquippedStack(EquipmentSlot.HEAD);

        }
    }

}
