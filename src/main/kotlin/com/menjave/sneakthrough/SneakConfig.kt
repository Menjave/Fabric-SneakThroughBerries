package com.menjave.sneakthrough

import me.shedaniel.clothconfig2.api.AbstractConfigEntry
import me.shedaniel.clothconfig2.api.ConfigBuilder
import me.shedaniel.clothconfig2.api.ConfigCategory
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder
import net.minecraft.text.TranslatableText


/*
* Borrowed wording from the old version.
* Though everything is mostly handwritten.
* The auto-generated kotlin code was a mess 💀 💀 💀 💀
* */


object SneakConfig {
    private var Builder: ConfigBuilder = ConfigBuilder.create()

    private var Boots: Boolean = false

    init {
        Builder.parentScreen = parent
        Builder.title = TranslatableText("SneakThrough.Berries.config")
        Builder.setSavingRunnable {
            val entry: ConfigEntryBuilder = Builder.entryBuilder()

            val general: ConfigCategory = Builder.getOrCreateCategory(TranslatableText("Armor.SneakThroughBerries.General"))
                .addEntry()
            }
    }
}

