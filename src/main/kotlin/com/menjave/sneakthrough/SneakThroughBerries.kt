package com.menjave.sneakthrough
import net.fabricmc.api.ModInitializer

@Suppress("UNUSED")
object SneakThroughBerries: ModInitializer {
    private const val MOD_ID = "sneakthrough"

    override fun onInitialize() {
    }
}